<?php

class m_entry extends mycelium_model {
    public function import_entry($term,$def,$kwd,$token){
        //cari module status = 2 id paling kecil parent != 0
        $rst = $this->runQuery("SELECT id FROM module WHERE status = 2 AND parent != 0 ORDER BY id ASC");
        $rs = $this->runQuery("INSERT INTO entry (module,term,def,kwd,user) VALUES (".$rst[0]['id'].",'$term','$def','$kwd','$token')");
        $now = date("Y-m-d H:i:s");
        $this->runQuery("INSERT INTO log (user,act,wkt) VALUES ('$token','add entry','$now')");  
        return "INSERT INTO entry (module,term,def,kwd,user) VALUES (".$rst[0]['id'].",'$term','$def','$kwd','$token')";
    }
    public function tambah_entry($mod,$term,$def,$kwd,$token){
        $rs = $this->runQuery("INSERT INTO entry (module,term,def,kwd,user) VALUES ($mod,'$term','$def','$kwd','$token')");
        $now = date("Y-m-d H:i:s");
        $this->runQuery("INSERT INTO log (user,act,wkt) VALUES ('$token','add entry','$now')");
        return $rs;
    }
    public function hapus_entry($id,$token){
        $rs = $this->runQuery("DELETE FROM entry WHERE id = $id");
        $now = date("Y-m-d H:i:s");
        $this->runQuery("INSERT INTO log (user,act,wkt) VALUES ('$token','delete entry','$now')");
        return $rs;
    }
    public function update_entry($mod,$term,$def,$kwd,$token,$id){
        $rs = $this->runQuery("UPDATE entry SET module = $mod, term = '$term', def = '$def', kwd = '$kwd' WHERE id = $id");
        $now = date("Y-m-d H:i:s");
        $this->runQuery("INSERT INTO log (user,act,wkt) VALUES ('$token','update entry','$now')");
        return $rs;
    }
    public function tabel_entry($lim){
        /*
        $limit = (empty($lim) || is_null($lim) || $lim == "")? "" : "LIMIT $lim,5";
        $limit = ($lim == 0 || $lim == "0")? "LIMIT 0,5" : "LIMIT $lim,5"; */

        $limit = "";
        if(empty($lim) || is_null($lim) || $lim == ""){}
        else if($lim == 0 || $lim == "0"){
            $limit = "LIMIT 0,5";
        } else {
            $limit = "LIMIT $lim,5";
        }
        $rs = $this->runQuery("SELECT a.id, a.term, CONCAT(LEFT(a.def,25),'...') defs, a.kwd, b.name AS subkat, c.name AS kat, a.module FROM entry a JOIN module b ON a.module = b.id JOIN module c ON b.parent = c.id ORDER BY a.id ASC $limit");   if(is_array($rs)){
            foreach($rs as $k => $v){
                 $ret[$k] = array_map(function($x){
                    return strip_tags($x);
                },$v);    
            }
            return $this->toJSON($ret);
           //return $rs;
          
        } else {
            return '{"rows":[]}';
        } 
      
    }
    public function cari_entry($id){
        $rs = $this->runQuery("SELECT * FROM entry WHERE id = $id");
        if(is_array($rs)){
          // var_dump($rs);
           // var_dump($this->toJSON($rs));
            return $this->toJSON($rs);
        } else {
            return '{"rows":[]}';
        }
    }
    public function hitung_kata(){
        $rs = $this->runQuery("SELECT COUNT(id) tot FROM entry");
        if(is_array($rs)){
            return $this->toJSON($rs);
        } else {
            return '{"rows":[]}';
        }
    }
    public function hitung($token){
        $rs = $this->runQuery("SELECT COUNT(id) tot FROM entry WHERE user = '$token'");
        if(is_array($rs)){
            return $this->toJSON($rs);
        } else {
            return '{"rows":[]}';
        }
    }
    public function cari_entry2($q){
        $rs = $this->runQuery("SELECT a.id, a.term, CONCAT(LEFT(a.def,25),'...') defs, a.kwd, b.name AS subkat, c.name AS kat, a.module FROM entry a JOIN module b ON a.module = b.id JOIN module c ON b.parent = c.id WHERE term LIKE '%$q%' ORDER BY a.id DESC");
        if(is_array($rs)){
            foreach($rs as $k => $v){
                $ret[$k] = array_map(function($x){
                    return strip_tags($x);
                },$v);
            }
            return $this->toJSON($ret);
        } else {
            return '{"rows":[]}';
        }
    }
    public function cari_saran(){
        $rs = $this->runQuery("SELECT * FROM suggest");
        if(is_array($rs)){
            return $this->toJSON($rs);
        } else {
            return '{"rows":[]}';
        }
    }
    public function hapus_saran($token,$id){
        $rs = $this->runQuery("DELETE FROM suggest WHERE id = $id");
        $now = date("Y-m-d H:i:s");
        $this->runQuery("INSERT INTO log (user,act,wkt) VALUES ('$token','delete suggest','$now')");
    }
}

?>