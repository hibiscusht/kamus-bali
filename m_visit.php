<?php

class m_visit extends mycelium_model {
    public function komente($token,$bls,$bls_id,$id){
        $now = date("Y-m-d");
        if($bls_id == ""){
            //insert balasan
            $this->runQuery("INSERT INTO komen (nama,ket,tgl,parent) VALUES ('$token','$bls','$now','$id')");
        } else {
            //update balasan
            $this->runQuery("UPDATE komen SET ket = '$bls', tgl = '$now' WHERE id = $bls_id");
        }
        $this->runQuery("INSERT INTO log (user,act,wkt) VALUES ('$token','replying comment','$now')");
    }
    public function komenti($id){
        $res = $this->runQuery("SELECT * FROM komen WHERE id = $id");
        $res2 = $this->runQuery("SELECT id AS bls_id, ket AS bls FROM komen WHERE parent = $id LIMIT 1");
        $ret = '{"rows":';
        $rets = array();
        array_push($rets,$res[0]);
        if(is_array($res2)){
            array_push($rets,$res2[0]);
        } 
        $ret .= json_encode($rets).'}';
        return $ret;
    }
    public function koment(){
        $res = $this->runQuery("SELECT * FROM komen");
        $rest = '{"rows":[';
        $ret = '';
        foreach($res as $row){
            if(is_null($row["status"])){
                $status = "<button class='btn btn-sm btn-danger' onclick='active(1,".$row["id"].")' id='".$row["id"]."'>Aktifkan</button>";
            } else {
                $status = "<button class='btn btn-sm btn-danger' onclick='active(2,".$row["id"].")' id='".$row["id"]."'>Nonaktifkan</button>";
            }
            $ret .= '{"id":"'.$row["id"].'","stat":"'.$status.'","nama":"'.$row["nama"].'","ket":"'.$row["ket"].'","parent":"'.$row["parent"].'"},';
        }
        $ret = substr($ret,0,strlen($ret) - 1);
        $rest .= $ret.']}';
        return $rest;
  }
  public function activekomen($token,$id,$type){
      if($type == "1"){
          $sql = "UPDATE komen SET status = '1' WHERE id = $id";
        $this->runQuery($sql);
      } else {
          $sql = "UPDATE komen SET status = NULL WHERE id = $id";
        $this->runQuery($sql);
      }
      $now = date("Y-m-d");
      $this->runQuery("INSERT INTO log (user,act,wkt) VALUES ('$token','changing user comment','$now')");
      return $sql;
  }
  public function komentdel($token,$id){
      $this->runQuery("DELETE FROM komen WHERE id = $id");
    $now = date("Y-m-d");
    $this->runQuery("INSERT INTO log (user,act,wkt) VALUES ('$token','changing user comment','$now')");
  }
    public function komen(){
        $res = $this->runQuery("SELECT * FROM komen ORDER BY id DESC LIMIT 5");
        $ret = '';
        $i = 0;
        $ret = 'tidak ada data';
        foreach($res as $row){
            if($i == 0){
                $ret = '<b>'.$row["nama"].' ('.$row["tgl"].')</b> <br/>'.$row["ket"].'<br/>';
            } else {
              $ret .= '<b>'.$row["nama"].' ('.$row["tgl"].')</b> <br/>'.$row["ket"].'<br/>';
            }
            $i++;
        }
        $rest = '{"jml":"'.$i.'","term":"'.$ret.'"}';
        return $rest;
  }
    public function sarkat(){
          $res = $this->runQuery("SELECT term FROM suggest ORDER BY id DESC LIMIT 5");
          $ret = '';
          $i = 0;
          $ret = 'tidak ada data';
          if(is_array($res)){
            foreach($res as $row){
                if($i == 0){
                    $ret = $row["term"].'<br/>';
                } else {
                  $ret .= $row["term"].'<br/>';
                }
                $i++;
            }
          }
          
          $rest = '{"jml":"'.$i.'","term":"'.$ret.'"}';
          return $rest;
    }
    public function jml_addr($id){
        switch($id){
            case 1: $where = ""; break;
            case 2: $dt = date("Y-m"); $where = "WHERE tgl LIKE '%$dt%'"; break;
            case 3: $dt = date("Y-m-d"); $where = "WHERE tgl LIKE '%$dt%'"; break;
        }
        $rs = $this->runQuery("SELECT COUNT(address) jml FROM visitor $where");
        $res = $rs[0]["jml"]; 
        return $res;
    }
    public function cari_addr($tgl){
        $rs = $this->runQuery("SELECT COUNT(address) jml, DATE(tgl) tg FROM visitor WHERE tgl LIKE '%$tgl%' GROUP BY DATE(tgl)");
        $jml = '[';
        $tgl = '[';
        foreach($rs as $row){
           $jml .= $row["jml"].',';
           $tgl .= '"'.$row["tg"].'",';
        }
        $jml = substr($jml,0,strlen($jml) - 1).']';
        $tgl = substr($tgl,0,strlen($tgl) - 1).']';
        $res = '{"labels":'.$tgl.',"data":'.$jml.'}';
        return $res;
    }
    public function simpan_addr($ip){
        $now = date("Y-m-d H:i:s");
        $this->runQuery("INSERT INTO visitor (tgl,address) VALUES('$now','$ip')");
    }
    public function hitung_addr($t1,$t2){ 
        $tg1 = "$t1 00:00:00";
        $tg2 = "$t2 23:59:59";
       $rs = $this->runQuery("SELECT COUNT(address) jml, address AS ip FROM visitor WHERE tgl >= '$tg1' AND tgl <= '$tg2' GROUP BY address");
        if(is_array($rs)){
            return $this->toJSON($rs);
        } else {
            return '{"rows":[]}';
        }  
    }
}

?>