<?php

class entries extends mycelium_controller {
    public function get_details($details){
        $det = explode("/",$details);
        return $det;
    }
    public function excel($dtl){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $this->load->library("Spreadsheet_Excel_Reader");
        $data = $this->Spreadsheet_Excel_Reader($_FILES['xls']['tmp_name']);
        
        $baris = $data->rowcount($sheet_index=0);
        $j = "";
        for ($i=2; $i<=$baris; $i++)
            {
                // membaca data id (kolom ke-1)
                $term = mb_convert_encoding($data->val($i, 1),"UTF-8","ISO-8859-1");
                $def = mb_convert_encoding($data->val($i, 2),"UTF-8","ISO-8859-1");
                $kwd = mb_convert_encoding($data->val($i, 3),"UTF-8","ISO-8859-1");
                $err = $data->val($i, 1);
                if($err == "" || empty($err) || is_null($err)){
                     
                } else { 
                    $rs = $this->db->import_entry($term,$def,$kwd,$dtl[1]);	
                } 
               
            }
        
    }
    public function suggestdel($dtl){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $rs = $this->db->hapus_saran($dtl[1],$dtl[3]);
        echo $rs;
    }
    public function suggests($dtl){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $rs = $this->db->cari_entry2($dtl[1]);
        echo $rs;
    }
    public function suggest(){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $rs = $this->db->cari_saran();
        echo $rs;
    }
    public function report($dtl){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $rs = $this->db->hitung($dtl[1]);
        echo $rs;
    }
    public function kata(){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $rs = $this->db->hitung_kata();
        echo $rs;
    }
    public function selectentry($dtl){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $rs = $this->db->cari_entry($dtl[3]);
        echo $rs;
    }
    public function entry($dtl){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $rs = $this->db->tabel_entry($dtl[1]);
        echo $rs;
    }
    public function entrydel($dtl){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $rs = $this->db->hapus_entry($dtl[3],$dtl[1]);
        if($rs != "0"){
            echo '{"status":"success"}';
        } else {
            echo '{"status":"'.$rs.'"}';
        }
    }
    public function entrysave($dtl){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $subkat = $this->input->post("subkat"); 
        $entry = mb_convert_encoding($this->input->post("entry"),"UTF-8","ISO-8859-1");
        $def = mb_convert_encoding($this->input->post("def"),"UTF-8","ISO-8859-1") ;
        $kwd = mb_convert_encoding($this->input->post("kwd"),"UTF-8","ISO-8859-1");
        $rs = $this->db->tambah_entry($subkat,$entry,$def,$kwd,$dtl[1]);
        if($rs != "0"){
            echo '{"status":"success"}';
        } else {
            echo '{"status":"'.$rs.'"}';
        }
    }
    public function entryupdate($dtl){
        $this->load->rules("admin");
        $this->load->model("m_entry");
        $subkat = $this->input->post("subkat");
        $entry = mb_convert_encoding($this->input->post("entry"),"UTF-8","ISO-8859-1");
        $def = mb_convert_encoding($this->input->post("def"),"UTF-8","ISO-8859-1") ;
        $kwd = mb_convert_encoding($this->input->post("kwd"),"UTF-8","ISO-8859-1");
        $eid = $this->input->post("eid");
        $rs = $this->db->update_entry($subkat,$entry,$def,$kwd,$dtl[1],$eid);
        if($rs != "0"){
            echo '{"status":"success"}';
        } else {
            echo '{"status":"'.$rs.'"}';
        }
    }
    public function index(){
        $details = $this->input->get("details");
        $dtl = $this->get_details($details);
        $method = $dtl[0];
        echo $this->$method($dtl);
    }
}

?>