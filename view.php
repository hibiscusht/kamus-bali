<?php
$base_url = "http://localhost/kb1/";
?>

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from colorlib.com/polygon/cooladmin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 03:41:05 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="au theme template">
<meta name="author" content="Hau Nguyen">
<meta name="keywords" content="au theme template">

<title>Kamus Bali Indonesia</title>

<link href="<?php echo $base_url; ?>view/css/font-face.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/wow/animate.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/slick/slick.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/select2/select2.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/css/theme.css" rel="stylesheet" media="all">

<script src="<?php echo $base_url; ?>view/vendor/jquery-3.2.1.min.js" type="text/javascript"></script>

</head>
<body class="animsition">

<?php
$det = explode("/",$_GET["details"]);
switch($det[0]){

    case "admin":

    ?>
   
<div class="page-wrapper" >
    <style>
        .lock {opacity: 0}
        .overview-item--cwt {
        background-image: -webkit-linear-gradient(90deg, #4272d7 0%, #4272d7 100%);}
        .transparent {background: rgba(0,0,0,0)}  
        .blue {background: #4272d7 !important}
        #my-chart {width: 885px !important; height: 150px !important}
        @media (max-width: 992px){ 
            .bluish {background: white !important}
            #my-chart {width: 379px !important; height: 162px !important}
            .hamburger {background: rgba(0,0,0,0) !important}
        }
    </style>
<div class="lock" id="locked" data-token="<?php echo $det[1]; ?>">
<header class="header-mobile blue d-block d-lg-none">
<div class="header-mobile__bar">
<div class="container-fluid">
<div class="header-mobile-inner">
<a class="logo" href="<?php echo $base_url; ?>spa/admin/<?php echo $det[1]; ?>/home">
<img style="" src=" " alt="@hibiscusht" class="imagery" />
</a>
<span style="color: white; font-weight: bold; width: 600px; margin-left: 3%; font-size: 1.1em">BALAI BAHASA PROVINSI BALI</span>
<button style="align-items: flex-end;
    float: right; 
    padding-top: 1%;
    margin-right: 5%; ">&nbsp;&nbsp;</button>
<button class="hamburger hamburger--slider" type="button">
<span class="hamburger-box">
<span class="hamburger-inner"></span>
</span>
</button>
</div>
</div>
</div>
<nav class="navbar-mobile">
<div class="container-fluid">
<ul class="navbar-mobile__list list-unstyled modules mobile">
</ul>
</div>
</nav>
</header>


<aside class="menu-sidebar d-none d-lg-block">
<div class="logo blue">
<a href="<?php echo $base_url; ?>spa/admin/<?php echo $det[1]; ?>/home">
<img style="" src=" " alt="@hibiscusht" class="imagery"/>
</a>
<span style="color: white; font-weight: bold; width: 400px; margin-left: 5%; font-size: 1.1em">BALAI BAHASA PROVINSI BALI</span>
</div>
<div class="menu-sidebar__content js-scrollbar1">
<nav class="navbar-sidebar">
<ul class="list-unstyled navbar__list modules">
</ul>
</nav>
 </div>
</aside>


<div class="page-container">

<header class="header-desktop blue bluish">
<div class="section__content section__content--p30">
<div class="container-fluid">
<div class="header-wrap">
<form class="form-header" action="#" method="POST">
<input class="au-input au-input--xl" type="text" id="q" name="search" placeholder="Cari entry kamus" />
<button class="au-btn--submit" type="button" onclick="carkat()">
<i class="zmdi zmdi-search"></i>
</button>
</form>
<div class="header-button">
<div class="noti-wrap">
<div class="noti__item js-item-menu" onclick="clrnotif('sarkat-i')">
<i class="zmdi zmdi-comment-more"></i>
<span class="quantity" id="sarkat-i"><i class="fa fa-spinner fa-pulse fa-fw text-white" style="font-size: 11.5px; "></i></span>
<div class="mess-dropdown js-dropdown">
<div class="mess__title">
<p>Saran Kata Belum Diterima</p>
</div>
<div class="mess__item" id="sarkat"> 
<div class="content progress"> 
<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%; background-color: #b8bcbf !important" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" ></div>
</div>
</div>
<div class="mess__footer">
<a href="#" onclick="unhides('suggest-table')">Lihat Semua</a>
</div>
</div>
</div>
<div class="noti__item js-item-menu"  onclick="clrnotif('komen-i')">
<i class="zmdi zmdi-email"></i>
<span class="quantity" id="komen-i"><i class="fa fa-spinner fa-pulse fa-fw text-white" style="font-size: 11.5px; "></i></span>
<div class="email-dropdown js-dropdown">
<div class="email__title">
<p>Komentar Pengunjung</p>
</div>
<div class="email__item"  style="height: 150px; overflow-y: auto"> 
<div id="komen" style="height: 100%">
<div class="content progress"> 
<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%; background-color: #b8bcbf !important" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" ></div>
</div>
</div>
</div>
<div class="email__footer">
<a href="#" onclick="unhides('koment-table')">Lihat Semua</a>
</div>
</div>
</div>
<!-- end -->
</div>
<div class="account-wrap">
<div class="account-item clearfix js-item-menu">
<div class="image">
<img class="profilpic" src="" alt="John Doe" />
</div>
<div class="content">
<a class="js-acc-btn username" href="#"> </a>
</div>
<div class="account-dropdown js-dropdown">
<div class="info clearfix">
<div class="image">
<a href="#">
<img src="" class="profilpic" alt="John Doe" />
</a>
</div>
<div class="content">
<h5 class="name">
<a href="#" class="username"> </a>
</h5>
<span class="email"><a href="https://colorlib.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="2e444146404a414b6e4b564f435e424b004d4143">[email&#160;protected]</a></span>
</div>
</div>
<div class="account-dropdown__body">
</div>
<div class="account-dropdown__footer">
<a href="<?php echo $base_url; ?>spa/logout/<?php echo $det[1]; ?>">
<i class="zmdi zmdi-power"></i>Logout</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</header>


<div class="main-content">
<div class="section__content section__content--p30">


<div class="container-fluid">
<div class="row global home">

<div class="col-lg-12">
<div class="card">
<div class="card-header overview-item--cwt"> 
<h4 style="color: #ffffff; text-shadow: rgba(0,0,0,0.5) -1px 0, rgba(0,0,0,0.3) 0 -1px, rgb(66 114 215) 0 1px, rgba(0,0,0,0.3) -1px -2px;">
<?php echo strftime("%B %d %Y",strtotime(date("Y-m-d"))); ?></h4>
 
</div>
<div class="au-task js-list-load">
<div class="au-task__title">
<p>Anda menambahkan <span id="tasks">0</span> entry kamus</p>
</div>

</div>
</div>
</div>

<!-- row -->
</div>

<div class="row global home">
<!-- banner 1 -->

<div class="col-sm-6 col-lg-3">
<div class="overview-item overview-item--c1">
<div class="overview__inner">
<div class="overview-box clearfix">
<div class="icon">
<i class="zmdi zmdi-account-o"></i>
</div>
<div class="text">
<h2 id="tot">0</h2>
<span>kunjungan total</span>
</div>
</div> 
</div>
</div>
</div>

<!-- banner 1 -->

<!-- banner 2 -->

<div class="col-sm-6 col-lg-3">
<div class="overview-item overview-item--c2">
<div class="overview__inner">
<div class="overview-box clearfix">
<div class="icon">
<i class="zmdi zmdi-account-o"></i>
</div>
<div class="text">
<h2 id="bln">0</h2>
<span>kunjungan bulan ini</span>
</div>
</div> 
</div>
</div>
</div>

<!-- banner 2 -->
  

<!-- banner 3 -->

<div class="col-sm-6 col-lg-3">
<div class="overview-item overview-item--c3">
<div class="overview__inner">
<div class="overview-box clearfix">
<div class="icon">
<i class="zmdi zmdi-account-o"></i>
</div>
<div class="text">
<h2 id="hari">0</h2>
<span>kunjungan hari ini</span>
</div>
</div> 
</div>
</div>
</div>

<!-- banner 3 -->

<!-- banner 4 -->

<div class="col-sm-6 col-lg-3">
<div class="overview-item overview-item--c4">
<div class="overview__inner">
<div class="overview-box clearfix">
<div class="icon">
<i class="zmdi zmdi-bookmark"></i>
</div>
<div class="text">
<h2 id="kms">0</h2>
<span>jumlah istilah kamus</span>
</div>
</div> 
</div>
</div>
</div>

<!-- banner 4 -->
</div>


<div class="row global home">
<div class="col-lg-12">
<div class="au-card m-b-30"> 
<h3 class="title-2 m-b-40">Grafik Pengunjung</h3>
<canvas id="my-chart" class="chartjs-render-monitor"></canvas>
</div>
</div>

</div>
<div class="col-lg-12 global home">
<div class="au-card au-card--no-shadow au-card--no-pad m-b-40 transparent">
<br/><br/><span class=""> 
</div>


</div>

<script>

async function set_desc(){
    let ah = await fetch("<?php echo $base_url; ?>module.php/admin/identy");
    let btp = await ah.json();
    if(ah.ok){ 
        $(".imagery").attr("src","<?php echo $base_url; ?>view/images/icon/" + btp[1].def);
    }
}

set_desc();

function clrnotif(id){
    $("#" + id).css("opacity","0");
}

async function carkats(){
    let q = $("#q").val();
    let ah = await fetch("<?php echo $base_url; ?>entries.php/admin/suggests/" + q);
    let btp = await ah.json();
    $(".remove").remove();
    $("#e-dt").append("<tr class='remove'><td colspan='2'><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</td></tr>");
    if(ah.ok){
        let jk = ""; 
        if(btp.rows.length == 0){
            jk = "<tr class='remove'><td colspan=2>tidak ada data</td></tr>";
        } else {
            for(let i in btp.rows){
                jk += "<tr class='remove'><td><button id='edit-" + btp.rows[i].id + "' class='btn btn-sm btn-info' onclick='edit(1,this.id)' data-type='entry'>Edit</button>&nbsp;<button id='del-" + btp.rows[i].id + "' class='btn btn-sm btn-danger' onclick='deletes(this.id)' data-type='entry'>Hapus</button></td><td>" + decodeURIComponent(btp.rows[i].term) + "</td><td>" + decodeURIComponent(btp.rows[i].defs) + "</td><td>" + decodeURIComponent(btp.rows[i].kwd) + "</td><td>" + btp.rows[i].subkat + "</td><td>" + btp.rows[i].kat + "</td></tr>";
            } 
        }  
        $(".remove").remove();
        $("#e-dt").append(jk);
    }
   
}

function carkat(){
    $(".global").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    }); 
          $("#entry-table").removeClass("hidden");
    carkats();
}

$(document).ready(function(){

async function push_notif(){
    let a = await fetch("<?php echo $base_url; ?>visit.php/admin/sarkat");
    let ab = await a.json();
    if(a.ok){
        if(ab.jml == "0"){
            $("#sarkat-i").css("opacity","0");
        }
        $("#sarkat-i").html(ab.jml);
        $("#sarkat").html(ab.term);
    }
    let b = await fetch("<?php echo $base_url; ?>visit.php/admin/komen");
    let bb = await b.json();
    if(b.ok){
        $("#komen-i").html(bb.jml);
        $("#komen").html(bb.term);
    }

}    

push_notif();

async function jml(){
    let ah = await fetch("<?php echo $base_url; ?>visit.php/admin/banner");
    let koh = await ah.json();
    if(ah.ok){
        $("#tot").html(koh.total);
        $("#bln").html(koh.bln);
        $("#hari").html(koh.hari);
    }
}   

jml();

async function chart(){

    let ah = await fetch("<?php echo $base_url; ?>visit.php/admin/chart");
    let koh = await ah.json();
    if(ah.ok){ 
        //Team chart
let ctx = document.getElementById("my-chart");
    if (ctx) {
      ctx.height = 150;
     let myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: koh.labels,
          type: 'line',
          defaultFontFamily: 'Poppins',
          datasets: [{
            data: koh.data,
            label: "Pengunjung",
            backgroundColor: 'rgba(0,103,255,.15)',
            borderColor: 'rgba(0,103,255,0.5)',
            borderWidth: 3.5,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(0,103,255,0.5)',
          },]
        },
        options: {
          responsive: true,
          tooltips: {
            mode: 'index',
            titleFontSize: 12,
            titleFontColor: '#000',
            bodyFontColor: '#000',
            backgroundColor: '#fff',
            titleFontFamily: 'Poppins',
            bodyFontFamily: 'Poppins',
            cornerRadius: 3,
            intersect: false,
          },
          legend: {
            display: false,
            position: 'top',
            labels: {
              usePointStyle: true,
              fontFamily: 'Poppins',
            },


          },
          scales: {
            xAxes: [{
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              scaleLabel: {
                display: false,
                labelString: 'Month'
              },
              ticks: {
                fontFamily: "Poppins"
              }
            }],
            yAxes: [{
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              scaleLabel: {
                display: true,
                labelString: 'Value',
                fontFamily: "Poppins"
              },
              ticks: {
                fontFamily: "Poppins"
              }
            }]
          },
          title: {
            display: false,
          }
        }
      });
    }

  

    }



}

chart();

});
</script>

<!-- end -->


</div>

<div class="container-fluid">

<!--- module kategori -->

<style>
.table-data th,.table-data tr {font-size: 0.85em !important}
.hidden {display: none}
</style>

<!-- tabel --> 

<div class="global hidden" id="kategori-table">

<div class="col-lg-12" style="margin-bottom: 5%; background: white; padding: 2%">
<div class="card-body">
<button type="button" class="btn btn-primary btn-sm" onclick="cycles('kategori-table','kategori-input')">
<i class="fa fa-plus-circle"></i>&nbsp; Tambah Kategori</button>
</div></div>

<div class="row">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Kategori Kamus</h3>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr>
<th>aksi</th>
<th>kategori</th>
</tr>
</thead>
<tbody id="kat-dt">
</tbody>
</table>
</div>
</div>
</div>
</div>

</div>
<!-- input -->

<div class="row global hidden" id="kategori-input">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Kategori</h3>
<input type="hidden" id="category-edit" class="clear"/>
<div class="form-group">
<label for="category" class="control-label mb-1">Kategori</label>
<input id="category" type="text" class="clear form-control" />
</div>
<div>
<button type="button" class="btn btn-sm btn-info">
<i class="fa fa-lock fa-lg"></i>&nbsp;
<span id="kat-simpan" onclick="katSimpan()">Simpan</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('kategori-input','kategori-table')">
<i class="fa fa-lock fa-lg"></i>&nbsp;
<span>Batal</span>
</button>
</div>
</div>
</div>
</div>

<!--- module kategori -->

<!--- module subkategori -->


<!-- tabel --> 

<div class="global hidden" id="subkategori-table">

<div class="col-lg-12" style="margin-bottom: 5%; background: white; padding: 2%">
<div class="card-body">
<button type="button" class="btn btn-primary btn-sm" onclick="cycles('subkategori-table','subkategori-input')">
<i class="fa fa-plus-circle"></i>&nbsp; Tambah Subkategori</button>
</div></div>

<div class="row">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Subkategori Kamus</h3>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr>
<th>aksi</th>
<th>subkategori</th>
<th>kategori</th>
</tr>
</thead>
<tbody id="sk-dt">
</tbody>
</table>
</div>
</div>
</div>
</div>

</div>
<!-- input -->

<div class="row global hidden" id="subkategori-input">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Subkategori</h3>
<input type="hidden" id="subcategory-edit" class="clear"/>
<div class="form-group">
<label for="subcategory-cat" class="control-label mb-1">Kategori</label>
<select id="subcategory-cat" class="form-control">
    <option value="">--silakan pilih--</option>
</select>
</div>
<div class="form-group">
<label for="subcategory" class="control-label mb-1">Subkategori</label>
<input id="subcategory" type="text" class="clear form-control" />
</div>
<div>
<button type="button" class="btn btn-sm btn-info">
<i class="fa fa-lock fa-lg"></i>&nbsp;
<span id="subkat-simpan" onclick="subkatSimpan()">Simpan</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('subkategori-input','subkategori-table')">
<i class="fa fa-lock fa-lg"></i>&nbsp;
<span>Batal</span>
</button>
</div>
</div>
</div>
</div>
<!--- module subkategori -->

<!--- module entry -->
<!-- tabel --> 

<div class="global hidden" id="entry-table">

<div class="col-lg-12" style="margin-bottom: 5%; background: white; padding: 2%">
<div class="card-body">
<button type="button" class="btn btn-primary btn-sm" onclick="cycles('entry-table','entry-input')">
<i class="fa fa-plus-circle"></i>&nbsp; Tambah Entry</button>

<button type="button" class="btn btn-success btn-sm" >
<label for="xls" style="margin: 0" id="imp" onclick="awaiting()"><i class="fa fa-upload"></i>&nbsp;Import XLS</label></button>
<input type="file" id="xls" style="display: none" onchange="upload_xls()"/>
 <button onclick="download()" type="button" class="btn btn-success btn-sm">
 <i class="fa fa-download"></i>&nbsp;Contoh XLS</button> 
</div></div>

<div class="row">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Entry Kamus</h3>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr>
<th>aksi</th>
<th>istilah</th>
<th>definisi</th>
<th>istilah terkait</th>
<th>subkategori</th>
<th>kategori</th>
</tr>
</thead>
<tbody id="e-dt">
</tbody>
</table>
</div>

<div style="margin: auto; display: flex; flex-direction: row; width: 40%" id="page">
</div>


</div>
</div>
</div>

</div>
<!-- input -->

<div class="row global hidden" id="entry-input">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Entry Kamus</h3>
<input type="hidden" id="entry-edit" class="clear"/>
<div class="form-group">
<label for="entry-subcat" class="control-label mb-1">Subkategori</label>
<select id="entry-subcat" class="form-control">
    <option value="">--silakan pilih--</option>
</select>
</div>
<div class="form-group">
<label for="entry" class="control-label mb-1">Istilah</label>
<input id="entry" type="text" class="clear form-control" />
</div>
<div class="form-group">
<label for="entry-def" class="control-label mb-1">Definisi</label>
<textarea id="entry-def" type="text" class="clear form-control"></textarea>
</div>
<div class="form-group">
<label for="entry-kwd" class="control-label mb-1">Istilah Terkait (dipisahkan dengan koma)</label>
<input id="entry-kwd" type="text" class="clear form-control" />
</div>
<div>
<button type="button" class="btn btn-sm btn-info">
<i class="fa fa-lock fa-lg"></i>&nbsp;
<span id="entry-simpan" onclick="entrySimpan()">Simpan</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('entry-input','entry-table')">
<i class="fa fa-lock fa-lg"></i>&nbsp;
<span>Batal</span>
</button>
</div>
</div>
</div>
</div>

<!-- include summernote css/js -->
<link href="<?php echo $base_url; ?>view/summernote/summernote.min.css" rel="stylesheet">
<script src="<?php echo $base_url; ?>view/summernote/summernote.min.js"></script>
<script>
$(document).ready(function() {
  $('#entry-def').summernote();
  $('#identitas-home').summernote();
  $('#identitas').summernote();
  //$('#koment-def').summernote();
});
</script>

<!--- module entry -->


<!--- module user -->
<!-- tabel --> 

<div class="global hidden" id="user-table">

<div class="col-lg-12" style="margin-bottom: 5%; background: white; padding: 2%">
<div class="card-body">
<button type="button" class="btn btn-primary btn-sm" onclick="cycles('user-table','user-input')">
<i class="fa fa-plus-circle"></i>&nbsp; Tambah Admin</button>
</div></div>

<div class="row">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Admin Kamus</h3>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr>
<th>aksi</th>
<th>nama</th>
<th>status</th> 
</tr>
</thead>
<tbody id="u-dt">
</tbody>
</table>
</div>
</div>
</div>
</div>

</div>
<!-- input -->

<div class="row global hidden" id="user-input">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Admin Kamus</h3>
<input type="hidden" id="user-edit" class="clear"/>
 
<div class="form-group">
<label for="user" class="control-label mb-1">Username</label>
<input id="user" type="text" class="clear form-control" />
</div>

<div class="form-group">
<label for="user-pwd" class="control-label mb-1">Password <span class="cek_edit"></span></label>
<input id="user-pwd" type="password" class="clear form-control" />
</div>

<div class="form-group">
<label for="user-sta" class="control-label mb-1">Status</label>
<select class="clear form-control" id="user-sta">
<option value="">--silakan pilih--</option>
<option value="0">Tidak Aktif</option>
<option value="1">Aktif</option>
</select>
</div>

<div class="form-group">
<label for="user-name" class="control-label mb-1">Nama Lengkap</label>
<input id="user-name" type="text" class="clear form-control" />
</div>

<div class="form-group">
<label for="user-pic" class="control-label mb-1">Profile Pic <span class="cek_edit"></span></label>
<input id="user-pic" type="file" class="clear form-control" />
</div>

<div>
<button type="button" class="btn btn-sm btn-info">
<span id="user-simpan" onclick="userSimpan()">
<i class="fa fa-lock fa-lg"></i>&nbsp;Simpan</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('user-input','user-table')">
<i class="fa fa-lock fa-lg"></i>&nbsp;
<span>Batal</span>
</button>
</div>
</div>
</div>
</div>

<!--- module user -->


<!--- module suggest -->
<!-- tabel --> 

<div class="global hidden" id="suggest-table">

<div class="card"></div>

<div class="row">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Saran Terjemahan</h3>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr>
<th>aksi</th>
<th>istilah</th>
<th>definisi</th>
<th>contoh</th>
</tr>
</thead>
<tbody id="su-dt">
</tbody>
</table>
</div>
</div>
</div>
</div>

</div>

<!--- module suggest --> 


<!--- module visit -->
<!-- tabel --> 

<div class="global hidden" id="visitor-table">

<div class="col-lg-12" style="margin-bottom: 5%; background: white; padding: 2%">
<div class="row">  
<div class="col-lg-4">Dari <input type="date" value="<?php echo date("Y-m-d"); ?>" id="t1" class="form-control"/></div><div class="col-lg-4">
Sampai <input type="date" value="<?php echo date("Y-m-d"); ?>" id="t2" class="form-control"/>
</div><div class="col-lg-4" style="padding-top: 4%">
<button onclick="dispopulate()" class="btn btn-warning">Cari</button>
</div> 
</div> 
</div>

<div class="row">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Laporan Pengunjung Per <span id="lap"></span></h3>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr> 
<th>iP address</th>
<th>jumlah</th>
</tr>
</thead>
<tbody id="v-dt">
</tbody>
</table>
</div>
</div>
</div>
</div>

</div>

<!--- module visit -->

<!--- module identitas -->
<!-- tabel --> 
 
<!-- input -->

<div class="row global hidden" id="identitas-table">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Tentang Kamus</h3>

<div class="form-group">
<label for="identitas" class="control-label mb-1">Logo Balai Bahasa (abaikan jika tidak diubah)</label>
<input type="file" id="identitas-file" class="form-control" />
</div>

<div class="form-group">
<label for="identitas" class="control-label mb-1">Kalimat Pembuka</label>
<textarea id="identitas-home" type="text" class="clear form-control" style="height: 120px"></textarea>
</div>
 
<div class="form-group">
<label for="identitas" class="control-label mb-1">Keterangan Kamus</label>
<textarea id="identitas" type="text" class="clear form-control" style="height: 120px"></textarea>
</div>
 
<div>
<button type="button" class="btn btn-sm btn-info">
<span id="identitas-simpan" onclick="identitasSimpan()"><i class="fa fa-lock fa-lg"></i>&nbsp;Simpan</span>
</button> 
</div>
</div>
</div>
</div>

<!--- module identitas -->
</div>

<!--- module koment -->
<!-- tabel --> 
 

<div class="global hidden" id="koment-table">
<div class="row">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Komentar Pengunjung</h3>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr> 
<th>aksi</th>
<th>balasan</th>
<th>aktivitas</th>
<th>nama</th>
<th>komentar</th>
</tr>
</thead>
<tbody id="kom-dt">
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>

<!-- input -->

<div class="row global hidden" id="koment-input">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Balasan</h3>
<input type="hidden" id="koment-edit" class="clear"/>
<input type="hidden" id="koment-bls-edit" class="clear"/>
 
<div class="form-group">
<label for="user" class="control-label mb-1">Nama</label> 
<p id="koment-nama"></p>
</div>

<div class="form-group">
<label for="user" class="control-label mb-1">Komentar</label> 
<p id="koment-ket"></p>
</div>

<div class="form-group">
<label for="user" class="control-label mb-1">Balasan</label> 
<textarea class="form-control" id="koment-def"></textarea>
</div>

<div>
<button type="button" class="btn btn-sm btn-info">
<span id="koment-simpan" onclick="komenSimpan()">
<i class="fa fa-lock fa-lg"></i>&nbsp;Balas</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('koment-input','koment-table')">
<i class="fa fa-lock fa-lg"></i>&nbsp;
<span>Batal</span>
</button>
</div>
</div>
</div>
</div>

<!--- module koment -->
</div>


<!-- end -->
</div>


</div>
</div>


</div>
</div>

</div>
<script>

function download(){
    window.open("<?php echo $base_url; ?>conidium/library/Spreadsheet_Excel_Reader/demo.xls");
}


function cycles(a,b){
    $("#" + b).removeClass("hidden");
    $("#" + a).addClass("hidden");
    $(".clear").val(""); 
    $(".cek_edit").html("");
        loadcat(a); 
    
}

async function active(type,id){
    $("#" + id).html("<i class='fa fa-spinner fa-pulse fa-fw'></i> memproses...");
    let f = await fetch("<?php echo $base_url; ?>visit.php/admin/activekomen/<?php echo $det[1]; ?>/" + id + "/" + type);
    if(f.ok){
        populate("koment-table");
    }
}

async function userSimpan(){
    $("#user-simpan").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> menyimpan...");
    let edit = $("#user-edit").val();
    let uname = $("#user").val();
    let pwd = $("#user-pwd").val();
    let stt = $("#user-sta").val();
    let nama = $("#user-name").val(); 
    let pic = document.getElementById("user-pic").files[0]; 
    let fd = new FormData();
    if(typeof pic === "undefined"){
            fd.append("uname",uname);
            fd.append("pwd",pwd);
            fd.append("stt",stt);
            fd.append("nama",nama);
        } else {
            fd.append("uname",uname);
            fd.append("pwd",pwd);
            fd.append("stt",stt);
            fd.append("nama",nama);
            fd.append("pic",pic);
        }
    if(edit == ""){
        //insert
        let a = await fetch("<?php echo $base_url;?>users.php/admin/add/<?php echo $det[1] ?>",{
            method: "POST",
            body: fd
        });
        if(a.ok){
            $("#user-simpan").html("<i class='fa fa-lock fa-lg'></i>&nbsp;Simpan");
            cycles("user-input","user-table");
            populate("user-table");
        }  
    } else {
        fd.append("edit",edit);
        if(typeof pic === "undefined"){
            fd.append("uname",uname);
            fd.append("pwd",pwd);
            fd.append("stt",stt);
            fd.append("nama",nama);
        } else {
            fd.append("uname",uname);
            fd.append("pwd",pwd);
            fd.append("stt",stt);
            fd.append("nama",nama);
            fd.append("pic",pic);
        }
        //update
        let a = await fetch("<?php echo $base_url;?>users.php/admin/update/<?php echo $det[1] ?>",{
            method: "POST",
            body: fd
        });
        if(a.ok){
            $("#user-simpan").html("<i class='fa fa-lock fa-lg'></i>&nbsp;Simpan");
            cycles("user-input","user-table");
            populate("user-table");
        }  
    }
}

async function identitasEdit(){
    $("#identitas-home").summernote('reset');
    $("#identitas").summernote('reset');
    let ah = await fetch("<?php echo $base_url; ?>module.php/admin/identy/");
    let j = await ah.json();
    if(ah.ok){
        $("#identitas").summernote('pasteHTML',j[0].def); 
        $("#identitas-home").summernote('pasteHTML',j[2].def);
        
    }
}

async function identitasSimpan(){
    $("#identitas-simpan").attr("disabled",true);
    $("#identitas-simpan").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> menyimpan...");
    let f = $("#identitas").val();
    let g = document.getElementById("identitas-file").files[0];
    let h = $("#identitas-home").val();
    let fd = new FormData();
    fd.append("ident",f);
    fd.append("home",h);
    fd.append("file",g);
    let ah = await fetch("<?php echo $base_url; ?>module.php/admin/ident/<?php echo $det[1]; ?>",{
        method: "POST",
        body: fd
    });
    if(ah.ok){
       location.reload();
    }

}

function awaiting(){
}

async function upload_xls(){
    $("#imp").html("<i class='fa fa-spinner fa-spin'></i>&nbsp;mengimpor...");
    let f = document.getElementById("xls").files[0];
    let fd = new FormData();
    fd.append("xls",f);
    let ah = await fetch("<?php echo $base_url; ?>entries.php/admin/excel/<?php echo $det[1]; ?>",{
        method: "POST",
        body: fd
    });
    if(ah.ok){
        alert("impor berhasil");
        $("#imp").html("<i class='fa fa-upload'></i>&nbsp;Impor XLS");
        jks = "";
        populate("entry-table");
    }

}

async function dispopulate(){

    let t1 = $("#t1").val();
    let t2 = $("#t2").val();
     
    let dt1 = Intl.DateTimeFormat("id-ID",{month: "short", day: "2-digit", year: "numeric"}).format(new Date(t1));
    let dt2 = Intl.DateTimeFormat("id-ID",{month: "short", day: "2-digit", year: "numeric"}).format(new Date(t2));
    $("#lap").html(dt1 + " &mdash; " + dt2);
    $(".remove").remove();
    $("#v-dt").append("<tr class='remove'><td colspan='2'><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</td></tr>");
    
    let fd = new FormData();
    fd.append("t1",t1);
    fd.append("t2",t2);
    let c = await fetch("<?php echo $base_url; ?>visit.php/admin/report",{
        method: "POST",
        body: fd
    });
    let btp = await c.json();
    if(c.ok){
        let jk = ""; 
        if(btp.rows.length == 0){
            jk = "<tr class='remove'><td colspan=2>tidak ada data</td></tr>";
        } else {
            for(let i in btp.rows){
                jk += "<tr class='remove'><td>" + btp.rows[i].ip + "</td><td>" + btp.rows[i].jml + "</td></tr>";
            } 
        }  
        $(".remove").remove();
        $("#v-dt").append(jk);
    }
}

async function terima(id){
    let ids = id.split("-");
    let ask = confirm("anda akan dialihkan ke module Entry Kamus. saran terjemahan yang anda pilih akan terhapus. untuk kembali ke module ini silakan buka dari menu utama");
    if(ask){
        $("#" + id).html("Terima");
    populate("entry-table");
    cycles("suggest-table","entry-input");
    loadcat("entry-table");
    let term = $("#" + id).data("term");
    let def = $("#" + id).data("def"); 
    $("#entry").val(term);
    $("#entry-def").val(def);
    let ku = await fetch("<?php echo $base_url; ?>entries.php/admin/suggestdel/<?php echo $det[1]; ?>/id/" + ids[1]);
    }  
    
}

async function loadcat(id){
    let uri = "";
    let uric = ""
    switch(id){
        case "subkategori-table": uri = "category"; uric = "subcategory-cat"; break;
        case "entry-table": uri = "subcategory"; uric = "entry-subcat"; break;
    }
    if(uri != ""){
        let ah = await fetch("<?php echo $base_url; ?>module.php/admin/" + uri);
    let jk = await ah.json();
    if(ah.ok){
        let m = "";
        $(".unloads").remove();
        for(let i in jk.rows){
            if(uri == "subcategory"){
                m += "<option class='unloads' value='" + jk.rows[i].id + "'>" + jk.rows[i].subkat + "</option>";
            } else {
                m += "<option class='unloads' value='" + jk.rows[i].id + "'>" + jk.rows[i].name + "</option>";
            }
           
        }
        $("#" + uric).append(m);
    }
    }
    let r = "ok";
    return r;
}

async function deletes(id){
    let ids = id.split("-");
    let type = $("#" + id).data("type");
    let uri = "";
    let pop = "";
    let md = ""
    if(type == "category"){
        uri = "catdel";
        pop = "kategori-table";
        md = "module.php";
        }
        else if(type == "subcategory"){
        uri = "subcatdel";
        pop = "subkategori-table";   
        md = "module.php"; 
        }
        else if(type == "entry"){
        uri = "entrydel";
        pop = "entry-table";  
        md = "entries.php";
        }
        else if(type == "identitas"){}
        else if(type == "user"){
            uri = "obliterate";
        pop = "user-table";  
        md = "users.php";
        }
        else if(type == "suggest"){
        uri = "suggestdel";
        pop = "suggest-table";  
        md = "entries.php";
        }
        else if(type == "koment"){
        uri = "komentdel";
        pop = "koment-table";  
        md = "visit.php";
        }

        let ask = confirm("apakah yakin menghapus?");
        if(ask){
            $("#" + id).html("<i class='fa fa-spinner fa-pulse fa-fw'></i> menghapus...");
            let k = await fetch("<?php echo $base_url; ?>" + md + "/admin/" + uri + "/<?php echo $det[1]; ?>/id/" + ids[1]);
        if(k.ok){
            populate(pop);
        }
        } 
}

async function komenSimpan(){
    $("#koment-simpan").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> membalas...");
    let fd = new FormData();
    let bls_id = $("#koment-bls-edit").val();
    let id = $("#koment-edit").val();
    let bls = $("#koment-def").val();
    fd.append("bls_id",bls_id);
    fd.append("id",id);
    fd.append("bls",bls);
    let k = await fetch("<?php echo $base_url; ?>visit.php/admin/komentbls/<?php echo $det[1]; ?>",{
        method:"POST",
        body: fd
    });
    if(k.ok){
        $("#koment-simpan").html("<i class='fa fa-lock fa-lg'></i>&nbsp;Balas");
        cycles("koment-input","koment-table");
        populate("koment-table");
    }
}

async function edit(state,id){
   
    let ids = id.split("-");
    let type = $("#" + id).data("type");
    if(state == 0){
        //tanpa ajax
        if(type == "category"){
            cycles("kategori-table","kategori-input");
            let input = $("#" + id).data("input");
            $("#" + type + "-edit").val(ids[1]);
            $("#" + type).val(input);
        }
        else if(type == "subcategory"){
            cycles("subkategori-table","subkategori-input");
            let input = $("#" + id).data("input");
            let kat = $("#" + id).data("kat");
            $("#" + type + "-edit").val(ids[1]);
            $("#" + type).val(input);
            let lc = await loadcat("subkategori-table");
            if(lc == "ok"){
                $("#" + type + "-cat").val(kat);
            }
           
        } 
       
    } else {
 
        
        if(type == "entry"){
            $("#entry-def").summernote('reset');
           let o = await fetch("<?php echo $base_url; ?>entries.php/admin/selectentry/<?php echo $det[1]; ?>/id/" + ids[1]);
           let p = await o.json();
           if(o.ok){
            cycles("entry-table","entry-input");
            $("#entry-edit").val(ids[1]);
           $("#entry").val(decodeURIComponent(p.rows[0].term));
           let lc = await loadcat("entry-table");
            if(lc == "ok"){
                $("#entry-subcat").val(p.rows[0].module);
            }
           $("#entry-def").summernote('pasteHTML',decodeURIComponent(p.rows[0].def));
           $("#entry-kwd").val(decodeURIComponent(p.rows[0].kwd));
           }
           
        }
        else if(type == "identitas"){}
            else if(type == "user"){
            cycles("user-table","user-input");
            $("#" + type + "-edit").val(ids[1]);
            $(".cek_edit").html("(kosongkan jika tidak diubah)");
            let k = await fetch("<?php echo $base_url; ?>users.php/admin/edituser/" + ids[1]);
            let ku = await k.json();
            if(k.ok){
                $("#user").val(ku.rows[0].username);
                $("#user-sta").val(ku.rows[0].status);
                $("#user-name").val(ku.rows[0].nama);
            }
            
        } else if(type == "koment"){
            cycles("koment-table","koment-input");
            $("#" + type + "-edit").val(ids[1]);
           // $("#koment-def").summernote("reset");
            let k = await fetch("<?php echo $base_url; ?>visit.php/admin/komenti/" + ids[1]);
            let ku = await k.json();
            if(k.ok){ 
                $("#koment-nama").html(ku.rows[0].nama);
                $("#koment-ket").html(ku.rows[0].ket);
                if(ku.rows.length > 1){
                $("#koment-bls-edit").val(ku.rows[1].bls_id);
               // $("#koment-def").summernote("pasteHTML",ku.rows[1].bls);
               $("#koment-def").val(ku.rows[1].bls);
                }
               
            }
        }



    }
}

let e_lim = 0;
let jks = "";
async function populate(id){
    
    let jk = "";
    let chk = "";
    let khc = "";
    let md = "";
          switch(id){
              case "kategori-table": chk = "category"; khc = "kat-dt"; md = "module.php"; break;
              case "subkategori-table": chk = "subcategory";  khc = "sk-dt"; md = "module.php"; break;
              case "entry-table": chk = "entry/" + e_lim;  khc = "e-dt"; md = "entries.php"; break;
            //  case "identitas-table": chk = "identitas";  khc = "i-dt"; md = "module.php"; break;
              case "user-table": chk = "user";  khc = "u-dt"; md = "module.php"; break;
              case "suggest-table": chk = "suggest";  khc = "su-dt"; md = "entries.php"; break;
              case "visitor-table": chk = "report";  khc = "v-dt"; md = "visit.php"; let d = new Date(); let dt = Intl.DateTimeFormat("id-ID",{month: "short", day: "2-digit", year: "numeric"}).format(d); $("#lap").html(dt + " &mdash; " + dt); break;
              case "koment-table": chk = "koment";  khc = "kom-dt"; md = "visit.php"; break;
          }
          $("#" + khc).append("<tr class='remove'><td colspan='2'><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</td></tr>");
        let ah = await fetch("<?php echo $base_url; ?>" + md + "/admin/" + chk);
        let btp = await ah.json();
        if(ah.ok){
            $(".remove").remove();
            if(btp.rows.length == 0){
                if(chk == "entry/" + e_lim){
                    e_lim = 0;
                } else {
                    jk = "<tr class='remove'><td colspan=2>tidak ada data</td></tr>";
                }
                
            } else {

                for(let i in btp.rows){
                    if(chk == "category"){

                        jk += "<tr class='remove'><td><button id='edit-" + btp.rows[i].id + "' data-input='" + btp.rows[i].name + "' class='btn btn-sm btn-info' onclick='edit(0,this.id)' data-type='" + chk + "'>Edit</button>&nbsp;<button id='del-" + btp.rows[i].id + "' class='btn btn-sm btn-danger' onclick='deletes(this.id)' data-type='" + chk + "'>Hapus</button></td><td>" + btp.rows[i].name + "</td></tr>";

                    } else if(chk == "subcategory"){
                        
                        jk += "<tr class='remove'><td><button id='edit-" + btp.rows[i].id + "' data-input='" + btp.rows[i].subkat + "' class='btn btn-sm btn-info' onclick='edit(0,this.id)' data-kat='" + btp.rows[i].katid + "' data-type='" + chk + "'>Edit</button>&nbsp;<button id='del-" + btp.rows[i].id + "' class='btn btn-sm btn-danger' onclick='deletes(this.id)' data-type='" + chk + "'>Hapus</button></td><td>" + btp.rows[i].subkat + "</td><td>" + btp.rows[i].kat + "</td></tr>";

                    } else if(chk == "entry/" + e_lim){

                        jks += "<tr class='remove'><td><button id='edit-" + btp.rows[i].id + "' class='btn btn-sm btn-info' onclick='edit(1,this.id)' data-type='entry'>Edit</button>&nbsp;<button id='del-" + btp.rows[i].id + "' class='btn btn-sm btn-danger' onclick='deletes(this.id)' data-type='entry'>Hapus</button></td><td>" + decodeURIComponent(btp.rows[i].term) + "</td><td>" +  decodeURIComponent(btp.rows[i].defs) + "</td><td>" + decodeURIComponent(btp.rows[i].kwd) + "</td><td>" + btp.rows[i].subkat + "</td><td>" + btp.rows[i].kat + "</td></tr>";

                    } else if(chk == "identitas"){

                    } else if(chk == "user"){

                        jk += "<tr class='remove'><td><button id='user-" + btp.rows[i].id + "' class='btn btn-sm btn-info' onclick='edit(1,this.id)' data-type='" + chk + "'>Edit</button>&nbsp;<button id='del-" + btp.rows[i].id + "' class='btn btn-sm btn-danger' onclick='deletes(this.id)' data-type='" + chk + "'>Hapus</button></td><td>" + btp.rows[i].nama + "</td><td>" + btp.rows[i].stt + "</td></tr>";

                    } else if(chk == "suggest"){

                        jk += "<tr class='remove'><td><button id='terima-" + btp.rows[i].id + "' class='btn btn-sm btn-info' onclick='terima(this.id)' data-term='" + btp.rows[i].term + "' data-def='" + btp.rows[i].def + "'>Terima</button>&nbsp;<button id='del-" + btp.rows[i].id + "' class='btn btn-sm btn-danger' onclick='deletes(this.id)' data-type='" + chk + "'>Hapus</button></td><td>" + btp.rows[i].term + "</td><td>" + btp.rows[i].def + "</td><td>" + btp.rows[i].btm + "</td></tr>";

                    } else if(chk == "report"){

                        jk += "<tr class='remove'><td>" + btp.rows[i].ip + "</td><td>" + btp.rows[i].jml + "</td></tr>";

                    } else if(chk == "koment"){
 
                        let bls = (btp.rows[i].parent != "")? "<span class='badge badge-success'>Balasan</span>" : "<button class='btn btn-sm btn-danger' onclick='edit(1,this.id)' id='koment-" + btp.rows[i].id + "'  data-type='" + chk + "'>Balas</button>";

                        let nm = (btp.rows[i].parent != "")? "<span class='badge badge-success'>Admin</span>" : btp.rows[i].nama;

                        jk += "<tr class='remove'><td><button id='del-" + btp.rows[i].id + "' class='btn btn-sm btn-danger' onclick='deletes(this.id)' data-type='" + chk + "'>Hapus</button></td><td>" + bls + "</td><td>" + btp.rows[i].stat + "</td><td>" + nm + "</td><td>" + btp.rows[i].ket + "</td></tr>";

                        }  
            }
            }
            if(chk == "entry/" + e_lim){ 
                
            }

            if(khc == "e-dt"){
                $("#" + khc).append(jks); 
                jks = "";
            } else {
                $("#" + khc).append(jk); 
                jks = "";
            }
            
        }
}
 let a = 1;

function left(){
     e_lim = (e_lim == 0)? 0 : e_lim - (a * 5);
    a = (a == 1)? 1 : a - 5;
    paging(); 
    populate("entry-table");
}

function right(){
     e_lim = (e_lim == 0)? 0 : e_lim + (a * 5);
    a = a + 5;
    paging();
    populate("entry-table");
}

function center(lim){
    e_lim = ((lim - 1) * 5);
    populate("entry-table");
}

function paging(){
    let e = "";
    e += "<div style='flex: 1; margin: 0 2% 0 2%; height: 50px; border-radius: 50%; border: solid 2px black; text-align: center; line-height: 38pt; font-size: 2em; color: white; background: #ff4b5a; cursor: pointer' onclick='left()'>" + a + "</div>";
    e += "<div style='flex: 1; margin: 0 2% 0 2%; height: 50px; border-radius: 50%; border: solid 2px black; text-align: center; line-height: 38pt; font-size: 2em; color: white; background: #ff4b5a; cursor: pointer' onclick='center(" + (a + 1) + ")'>" + (a + 1) + "</div>";
    e += "<div style='flex: 1; margin: 0 2% 0 2%; height: 50px; border-radius: 50%; border: solid 2px black; text-align: center; line-height: 38pt; font-size: 2em; color: white; background: #ff4b5a; cursor: pointer' onclick='center(" + (a + 2) + ")'>" + (a + 2) + "</div>";
    e += "<div style='flex: 1; margin: 0 2% 0 2%; height: 50px; border-radius: 50%; border: solid 2px black; text-align: center; line-height: 38pt; font-size: 2em; color: white; background: #ff4b5a; cursor: pointer' onclick='center(" + (a + 3) + ")'>" + (a + 3) + "</div>";
    e += "<div style='flex: 1; margin: 0 2% 0 2%; height: 50px; border-radius: 50%; border: solid 2px black; text-align: center; line-height: 38pt; font-size: 2em; color: white; background: #ff4b5a; cursor: pointer' onclick='right()'>" + (a + 4) + "</div>";
    
    $("#page").html(e);

}
paging();

async function katSimpan(){
    let act = ($("#category-edit").val() == "")? "catsave" : "catupdate";
    let kat = $("#category").val();
    let katid = $("#category-edit").val();
    let fd = new FormData();
    fd.append("kat",kat);
    fd.append("katid",katid);
    let j = await fetch("<?php echo $base_url; ?>module.php/admin/" + act + "/<?php echo $det[1]; ?>",{
        method: "POST",
        body: fd
    });
    let k = await j.json();
    if(k.status == "success"){
        cycles("kategori-input","kategori-table");
        populate("kategori-table");
    } 

}

async function subkatSimpan(){
    let act = ($("#subcategory-edit").val() == "")? "subcatsave" : "subcatupdate";
    let kat = $("#subcategory-cat").val();
    let subkat = $("#subcategory").val();
    let subkatid = $("#subcategory-edit").val();
    let fd = new FormData();
    fd.append("kat",kat);
    fd.append("subkat",subkat);
    fd.append("subkatid",subkatid);
    let j = await fetch("<?php echo $base_url; ?>module.php/admin/" + act + "/<?php echo $det[1]; ?>",{
        method: "POST",
        body: fd
    });
    let k = await j.json();
    if(k.status == "success"){
        cycles("subkategori-input","subkategori-table");
        populate("subkategori-table");
    } 

}

async function entrySimpan(){
    let act = ($("#entry-edit").val() == "")? "entrysave" : "entryupdate";
    let subkat = $("#entry-subcat").val();
    let entry = encodeURIComponent($("#entry").val());
    let def = encodeURIComponent($("#entry-def").val());
    let kwd = encodeURIComponent($("#entry-kwd").val());
    let eid = $("#entry-edit").val();
    let fd = new FormData();
    fd.append("subkat",subkat);
    fd.append("entry",entry);
    fd.append("def",def);
    fd.append("kwd",kwd);
    fd.append("eid",eid);
    let j = await fetch("<?php echo $base_url; ?>entries.php/admin/" + act + "/<?php echo $det[1]; ?>",{
        method: "POST",
        body: fd
    });
    let k = await j.json();
    if(k.status == "success"){
        cycles("entry-input","entry-table");
        populate("entry-table");
    } 

}

async function unhides(id){
    $(".global").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    });
         
          $(".hamburger").removeClass("is-active");
          $(".navbar-mobile").hide();
          $("#" + id).removeClass("hidden");

            populate(id);
            if(id == "identitas-table"){
                identitasEdit();
            }

        }

    $(document).ready(function(){

        async function get_module(){
            let a = await fetch("<?php echo $base_url; ?>module.php/admin/mod");
            let b = await a.json();
            if(a.ok){
                let mdl = "";
                for(let i in b.rows){
                    if(b.rows[i].name == "Home"){
                        mdl += "<li><a href='" + b.rows[i].link + "'><i class='fa fa-folder'></i>" + b.rows[i].name + "</a></li>";
                    } else {
                        let lnk = '"' + b.rows[i].link + '"';
                        mdl += "<li onclick='unhides(" + lnk + ")'><a href='#'><i class='fa fa-folder'></i>" + b.rows[i].name + "</a></li>";
                    }
                    
                }
                $(".modules").append(mdl);
            }
        }
        get_module();

        async function cek_login(){
            let token = $("#locked").data("token");
            let chk = await fetch("<?php echo $base_url; ?>users.php/admin/check/" + token);
            let chku = await chk.json();
            if(chku.status == "login again"){
                alert("anda belum login");
                location.href="<?php echo $base_url; ?>spa/login";
            } else {
                $(".lock").css("opacity","1");
            }
        }
        cek_login();

        async function users(){
            let token = $("#locked").data("token");
            let chk = await fetch("<?php echo $base_url; ?>users.php/admin/edit/" + token);
            let chku = await chk.json();
            let pics = (chku.rows[0].pic == null || chku.rows[0].pic == "NULL")? "view/images/icon/avatar-01.jpg" : "view/img/" + chku.rows[0].pic;
            $(".profilpic").attr("src","<?php echo $base_url; ?>" + pics); 
            $(".username").html(chku.rows[0].nama);
            $(".home").removeClass("hidden");
             let ku = await fetch("<?php echo $base_url; ?>entries.php/admin/report/" + token);
             let kuk = await ku.json();
            $("#tasks").html(kuk.rows[0].tot);
            let kus = await fetch("<?php echo $base_url; ?>entries.php/admin/kata/");
             let kuks = await kus.json();
            $("#kms").html(kuks.rows[0].tot);
           
           
        }
        users();
    });
</script>

    <?php
    break;
    case "users": break;
    case "login": 
    ?>

<div class="page-wrapper">
<div class="page-content--bge5">
<div class="container">
<div class="login-wrap">
<div class="login-content">
<div class="login-logo">
<a href="#">
<img src=" " alt="@hibiscusht" class="imagery">
</a>
</div>
<div class="login-form">
<form action="#" method="post">
<div class="form-group">
<label>Email Address</label>
<input class="au-input au-input--full" type="text" id="uname" placeholder="username">
</div>
<div class="form-group">
<label>Password</label>
<input class="au-input au-input--full" type="password" id="pwd" placeholder="password">
</div>

<button class="au-btn au-btn--block au-btn--green m-b-20" type="button" onclick="kirim()" id="send">Login</button>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

<script>
async function set_desc(){
    let ah = await fetch("<?php echo $base_url; ?>module.php/admin/identy");
    let btp = await ah.json();
    if(ah.ok){ 
        $(".imagery").attr("src","<?php echo $base_url; ?>view/images/icon/" + btp[1].def);
    }
}
set_desc();
    async function kirim(){
        $("#send").attr("disabled",true);
        $("#send").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> memvalidasi...");
        let uname = $("#uname").val();
        let pwd = $("#pwd").val();
        let hash = btoa(uname + ":" + pwd);
        let k = await fetch("<?php echo $base_url; ?>users.php/admin/validate/" + hash);
        let m = await k.json();
        if(m.status != "invalid"){
            $("#send").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> mengalihkan...");
            location.href="<?php echo $base_url; ?>spa/admin/" + m.status + "/home";
        } else {
            $("#send").attr("disabled",false);
            $("#send").html("Login");
            alert("username/password salah");
        }
    }
    </script>
   
    <?php
    break;
    case "logout": 
    ?>
    <script>
    async function send(){
        let hash = "<?php echo $det[1]; ?>";
        let k = await fetch("<?php echo $base_url; ?>users.php/admin/invalidate/" + hash);
        let j = await k.json();
        if(j.status == "login again"){
            alert("terimakasih telah menggunakan aplikasi kamus");
            location.href = "<?php echo $base_url; ?>spa/login";
        }
    }
    send();
    </script>
    <?php
    break;
    case "delete_user": 
    ?>
    <button onclick="send()" id="send">Delete User</button>
    <script>
    async function send(){
        let hash = "1";
        let k = await fetch("<?php echo $base_url; ?>users.php/admin/obliterate/" + hash);
    }
    </script>
    <?php
    break;
    case "public": 
   
    ?>

<style>
    table {border-collapse: separate}
    td a {font-size: 1.5em; color: white; -webkit-text-fill-color: white; -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: red }
    .hidden {display: none}
    .transparent {background: rgba(0,0,0,0)}
    .hide {display: block}
    .smaller {padding-top: 70px !important}
    .leptop {display: block}
    .hp {display: none}
    .blue {background: #4272d7 !important}
    .overview-item--crwt {
        background-image: -webkit-linear-gradient(90deg, #0c0c0c 0%, #ea940e 100%);
    }
    .overview-item--cwt {
        background-image: -webkit-linear-gradient(90deg, #4272d7 0%, #4272d7 100%);
    }
    .cool {width: 50% !important; position: relative; min-height: 1px; padding: 0 15px 0 15px}
    .padded {padding-top: 3% !important}
    .imagery2 {
        width: 100px;
        height: 100px;
        border: none; 
        background: url('../view/images/icon/kmasubali.png');
        background-size: 100% 100%;
        background-position: center
    }
    .lefty {
        margin-top: 2%;
        float: left;
        width: 45%;
    }
    .righty {
        margin-top: 2%;
        float: right;
        width: 45%;
    }
    @media (max-width: 991px){
        .leptop {display: none}
        .hp {display: block}
        .hide {display: none}
        .header-hide {margin-top: 15px !important}
        .search {height: 90px !important}
        .smaller {padding-top: 10px !important}
        .thinner {top: 75px !important}
        .thin-top {padding: 10px !important}
        .bluish {background: white !important}
        .hamburger {background: rgba(0,0,0,0) !important}
        .lefty {
        margin-bottom: 5%;
        width: 100%;
        float: left;
        }
        .righty {
        width: 100%;
        float: left;
        }
    }
    }
    </style>

<div class="page-wrapper">

<header class="header-mobile blue d-block d-lg-none">
<div class="header-mobile__bar thin-top">
<div class="container-fluid">
<div class="header-mobile-inner">
<a class="logo" href="<?php echo $base_url; ?>spa/public">
<img style="" src=" " alt="@hibiscusht" class="imagery" />
</a>
<span style="color: white; font-weight: bold; width: 600px; margin-left: 3%; font-size: 1.1em">BALAI BAHASA PROVINSI BALI</span>
<button style="align-items: flex-end;
    float: right;
    padding-top: 1%;
    margin-right: 5%; "><i class="zmdi zmdi-search" style="font-size: 1.5em" onclick="open_search()"></i></button>
<button class="hamburger hamburger--slider" type="button">
<span class="hamburger-box">
<span class="hamburger-inner"></span>
</span>
</button>
</div>
</div>
</div>
<nav class="navbar-mobile">
<div class="container-fluid">
<ul class="navbar-mobile__list list-unstyled modules">
</ul>
</div>
</nav>
</header>


<aside class="menu-sidebar d-none d-lg-block">
<div class="logo blue">
<a href="<?php echo $base_url; ?>spa/public">
<img style="" src=" " alt="@hibiscusht" class="imagery" />
</a><span style="color: white; font-weight: bold; width: 400px; margin-left: 5%; font-size: 1.1em">BALAI BAHASA PROVINSI BALI</span>
</div>
<div class="menu-sidebar__content js-scrollbar1">
<nav class="navbar-sidebar">
<ul class="list-unstyled navbar__list modules">
</ul>
</nav>
 </div>
</aside>


<div class="page-container thinner">

<header class="header-desktop blue hide search bluish">
<div class="section__content section__content--p30">
<div class="container-fluid">
<div class="header-wrap  header-hide">
<form class="form-header" action="#" method="POST">
<input class="au-input au-input--xl" id="q" type="text" name="search" placeholder="cari istilah di sini" />
<button class="au-btn--submit" type="button" onclick="searchs('')">
<i class="zmdi zmdi-search"></i>
</button>
</form>
<div class="header-button">
</div>
<!-- header button end -->
</div>
</div>
</div>
</header>


<div class="main-content smaller">
<div class="section__content section__content--p30 padded">

<div class="container-fluid">
    <div class="row m-t-25 hidden">
    <div class="col-sm-6 col-lg-12">
<div class="overview-item overview-item--crwt">
<div class="overview__inner"  style="margin-top: -4%">
<div class="overview-box clearfix" style="text-align: center; ">
<div class="icon" id="title" style="font-size: 1.24em; color: #6b4823; text-shadow: rgba(0,0,0,0.5) -1px 0, rgba(0,0,0,0.3) 0 -1px, rgb(111 74 8) 0 1px, rgba(0,0,0,0.3) -1px -2px;">
</div>
</div>
</div>
</div>
</div>
    </div>
    <!-- end -->
</div>

    <div class="container-fluid">

    <div class="row jdl">

<div class="col-lg-12">

<div class="card">
<div class="card-header hidden"><center>
<h5><span class="hp">Selamat Datang <br/> di Kamus Bali - Indonesia</span>
<span class="leptop">Selamat Datang di Kamus Bali - Indonesia</span></h5></center>
</div>
<div class="card-body"><center>
<div class="imagery2"></div>
<div></div><br/>
<center><p style="font-size: 1.2em"><strong>Kamus Bali &mdash; Indonesia <br/> Edisi Ke - 3</strong></p></center>
<!--<div style="max-width: 90px; overflow: hidden; text-indent: -5px">
<img style="max-width: 350px !important" src=" " alt="@hibiscusht" class="imagery"/></div>-->
<hr/>
<p id="homedesc"></p>
</center>
</div>
</div>

</div>

</div>


    <div class="row hidden">

    <div class="col-lg-12">

    <div class="card">
<div class="card-header">
<h4>Kamus Alfabetis</h4>
</div>
<div class="card-body">
<p class="text-muted m-b-15">Silakan pilih huruf awal istilah yang ingin anda lihat</p>
<div style="overflow-x: scroll">
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: 1100px">
<li class="nav-item">
<a class="nav-link" id="pills-a-tab" data-toggle="pill" href="#pills-a" role="tab" aria-controls="pills-a" aria-selected="true" onclick="searchs('a')">A</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pills-b-tab" data-toggle="pill" href="#pills-b" role="tab" aria-controls="pills-b" aria-selected="false" onclick="searchs('b')">B</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-c-tab" data-toggle="pill" href="#pills-c" role="tab" aria-controls="pills-c" aria-selected="false" onclick="searchs('c')">C</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-d-tab" data-toggle="pill" href="#pills-d" role="tab" aria-controls="pills-d" aria-selected="false" onclick="searchs('d')">D</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-e-tab" data-toggle="pill" href="#pills-e" role="tab" aria-controls="pills-e" aria-selected="false" onclick="searchs('e')">E</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-f-tab" data-toggle="pill" href="#pills-f" role="tab" aria-controls="pills-f" aria-selected="false" onclick="searchs('f')">F</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-g-tab" data-toggle="pill" href="#pills-g" role="tab" aria-controls="pills-g" aria-selected="false" onclick="searchs('g')">G</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-h-tab" data-toggle="pill" href="#pills-h" role="tab" aria-controls="pills-h" aria-selected="false" onclick="searchs('h')">H</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-i-tab" data-toggle="pill" href="#pills-i" role="tab" aria-controls="pills-i" aria-selected="false" onclick="searchs('i')">I</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-j-tab" data-toggle="pill" href="#pills-j" role="tab" aria-controls="pills-j" aria-selected="false" onclick="searchs('j')">J</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-k-tab" data-toggle="pill" href="#pills-k" role="tab" aria-controls="pills-k" aria-selected="false" onclick="searchs('k')">K</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-l-tab" data-toggle="pill" href="#pills-l" role="tab" aria-controls="pills-l" aria-selected="false" onclick="searchs('l')">L</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-m-tab" data-toggle="pill" href="#pills-m" role="tab" aria-controls="pills-m" aria-selected="false" onclick="searchs('m')">M</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-n-tab" data-toggle="pill" href="#pills-n" role="tab" aria-controls="pills-n" aria-selected="false" onclick="searchs('n')">N</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-o-tab" data-toggle="pill" href="#pills-o" role="tab" aria-controls="pills-o" aria-selected="false" onclick="searchs('o')">O</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-p-tab" data-toggle="pill" href="#pills-p" role="tab" aria-controls="pills-p" aria-selected="false" onclick="searchs('p')">P</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-q-tab" data-toggle="pill" href="#pills-q" role="tab" aria-controls="pills-q" aria-selected="false" onclick="searchs('q')">Q</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-r-tab" data-toggle="pill" href="#pills-r" role="tab" aria-controls="pills-r" aria-selected="false" onclick="searchs('r')">R</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-s-tab" data-toggle="pill" href="#pills-s" role="tab" aria-controls="pills-s" aria-selected="false" onclick="searchs('s')">S</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-t-tab" data-toggle="pill" href="#pills-t" role="tab" aria-controls="pills-t" aria-selected="false" onclick="searchs('t')">T</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-u-tab" data-toggle="pill" href="#pills-u" role="tab" aria-controls="pills-u" aria-selected="false" onclick="searchs('u')">U</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-v-tab" data-toggle="pill" href="#pills-v" role="tab" aria-controls="pills-v" aria-selected="false" onclick="searchs('v')">V</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-w-tab" data-toggle="pill" href="#pills-w" role="tab" aria-controls="pills-w" aria-selected="false" onclick="searchs('w')">W</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-x-tab" data-toggle="pill" href="#pills-x" role="tab" aria-controls="pills-x" aria-selected="false" onclick="searchs('x')">X</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-y-tab" data-toggle="pill" href="#pills-y" role="tab" aria-controls="pills-y" aria-selected="false" onclick="searchs('y')">Y</a>
 </li>
 <li class="nav-item">
<a class="nav-link" id="pills-z-tab" data-toggle="pill" href="#pills-z" role="tab" aria-controls="pills-z" aria-selected="false" onclick="searchs('z')">Z</a>
 </li>
</ul>
</div>

</div>
</div>


    </div>

    <!-- row end -->
    </div>
    <!-- container end -->
    </div>


<div class="container-fluid">

<div class="row global home hidden">
<div class="col-lg-12">
<div class="au-card au-card--no-shadow au-card--no-pad m-b-40 transparent">
<br/><br/><span class="await await-home"><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</span>
</div>
</div>
<!-- row -->
</div>

<div class="row global subcat hidden">
<div class="col-lg-12">
<div class="au-card au-card--no-shadow au-card--no-pad m-b-40 transparent">
<br/><br/><span class="await await-subcat"><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</span>
</div>
</div>
<!-- row -->
</div>

<div class="row global entry hidden">
<div class="col-lg-12">
<div class="au-card au-card--no-shadow au-card--no-pad m-b-40 transparent">
<br/><br/><span class="await await-entry"><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</span>
</div>
</div>
<!-- row -->
</div>

</div>
<!-- end -->


<div class="container-fluid">
    <div class="row hidden">

    <div class="col-lg-12">

    <div class="card">
<div class="card-header">
<h4>Penerjemah</h4>
</div>
<div class="card-body">
<p class="text-muted m-b-15">Silakan pilih mode terjemahan</p>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
<li class="nav-item">
<a class="nav-link t-mode active show" id="pills-1-tab" data-toggle="pill" href="#pills-1" role="tab" aria-controls="pills-1" aria-selected="true" onclick="change(1)">Indonesia&mdash;Bali</a>
</li>
<li class="nav-item">
<a class="nav-link t-mode" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab" aria-controls="pills-2" aria-selected="true" onclick="change(2)">Bali&mdash;Indonesia</a>
 </li>
 </ul>

<div class="row" style="padding-left: 5%; padding-right: 5%"> 
<div class="col" style=" margin-bottom: 15px">
<div class="row">
<textarea class="form-control" style="height: 150px; font-size: 0.85em" id="left"></textarea>
</div>
</div>

<div class="col" style="max-width: 1px !important">
<div class="row"><p>&nbsp;&nbsp;</p>
</div>
</div>

<div class="col" style=" margin-bottom: 15px">
<div class="row">
<textarea class="form-control" style="height: 150px; font-size: 0.85em" id="right"></textarea>
</div>
</div> 
</div>
<input type="hidden" id="mode" value="B-A"/>
<button class="btn btn-success" onclick="translates()">Terjemahkan</button>
<button class="btn btn-success" id="mod" onclick="solute()">Saran Kata >></button>

</div>
</div>


    </div>

    <!-- row end -->
    
    </div>

    <div class="row trans trans-main">

    <div class="col-lg-12">
<div class="card">
<div class="card-body">
<button class="btn btn-success lefty" onclick="solute(1)">Sarankan masukan entri/kata</button>
<button class="btn btn-success righty" onclick="solute(2)">Tentang Kamus</button>
</div>
</div>
</div> 
</div>


    <div class="row trans suggest hidden">

<div class="col-lg-12">

<div class="card">
<div class="card-header">
<h4>Sarankan masukan entri/kata</h4>
</div>
<div class="card-body">
<p class="text-muted m-b-15">Silakan masukan entri/kata</p>

<div class="row" style="padding-left: 5%; padding-right: 5%">
<div class="col" style=" margin-bottom: 15px">
<div class="row">
<input type="text" class="form-control" id="sug-left" placeholder="entri/kata" />
</div>
</div></div>
<div class="row" style="padding-left: 5%; padding-right: 5%">
<div class="col" style=" margin-bottom: 15px">
<div class="row">
<input type="text" class="form-control" id="sug-right" placeholder="padanan Indonesia"/>
</div>
</div>
</div>
<div class="row" style="padding-left: 5%; padding-right: 5%">
<div class="col" style=" margin-bottom: 15px">
<div class="row">
<input type="text" class="form-control" id="sug-bottom" placeholder="contoh kalimat bahasa bali"  />
</div>
</div></div> 
<button class="btn btn-success" id="sug" onclick="solution()">Kirim</button> </div>
</div>


</div>

<!-- row end -->

    </div>


    <div class="row trans abt hidden">

<div class="col-lg-12">

<div class="card">
<div class="card-header">
<h4>Tentang Kamus</h4>
</div>
<div class="card-body" id="0">
</div>
</div>
</div>
</div>
    <!-- container end -->
    <br/><br/><br/>
    </div>

<div id="template" style="display: none">
<div class="col-lg-12 destroy">
<div class="card">
<div class="card-header overview-item--cwt">
<div class=""></div>
<h4 style="color: #ffffff; text-shadow: rgba(0,0,0,0.5) -1px 0, rgba(0,0,0,0.3) 0 -1px, rgb(66 114 215) 0 1px, rgba(0,0,0,0.3) -1px -2px;">
{header}</h4> 
</div>
<div class="card-body">
<div class="au-task__title">
<p>{list}</p>
</div>
</div>
</div>
</div>
</div>

<div id="template2" style="display: none">
<div class="cool destroy">
<div class="card {cwt}" style="padding: 5%">
<h4 style="color: #243232">{header}</h4> 
<div class="card-body">
<div class="au-task__title" style="padding-left: 15px !important">
<p>{list}</p>
</div>
</div>
</div>
</div>
</div>


<!-- section__content end -->
</div>

</div>

<div style="position: fixed; bottom: 0; z-index: 9999; background: rgba(0,0,0,0.5); width: 100%; text-align: center; font-weight: bold; color: white; cursor: pointer" onclick="jmbthist()"><- Kembali 
</div>
<!-- page container end -->
</div>
<script>


async function set_desc(){
    let ah = await fetch("<?php echo $base_url; ?>module.php/admin/identy");
    let btp = await ah.json();
    if(ah.ok){
        $("#homedesc").html(btp[2].def);
        $(".imagery").attr("src","<?php echo $base_url; ?>view/images/icon/" + btp[1].def);
        $("#0").html(btp[0].def);
    }
}

set_desc();

let jmbtfwd = true;

function jmbtcbt(id,q){
    if(jmbtfwd){
    let jmbt = JSON.parse(localStorage.getItem("jmbt-hist"));
    let ids = Math.ceil(Math.random() * 10000);
    let hist = {id:ids,hist:id,q:q,gdl:""};
    jmbt.rows.push(hist);
    let json = JSON.stringify(jmbt);
    localStorage.removeItem("jmbt-hist");
    localStorage.setItem("jmbt-hist",json);
    jmbtfwd = true;
    } else {
        jmbtfwd = true;
    }
}

function open_search(){
    if($(".search").hasClass("hide")){
        $(".search").removeClass("hide");
    } else {
        $(".search").addClass("hide");
    }

}    

function change(mode){
    $(".t-mode").each(function(a,b){
        if($($(this)).hasClass("active show")){
            $($(this)).removeClass("active show");
        }
    });
    $("#pills-" + mode + "-tab").addClass("active show");
    if(mode == 1){
        $("#mode").val("B-A");
    } else {
        $("#mode").val("A-B");
    }
}
function desolute(){
    $(".trans").each(function(a,b){
        if($($(this)).hasClass("hidden")){ } else {
            $($(this)).addClass("hidden");
        }
    });
    $(".translate").removeClass("hidden");
}

function solute(id){
    $(".jdl").addClass("hidden");
    $(".trans").each(function(a,b){
        if($($(this)).hasClass("hidden")){ } else {
            $($(this)).addClass("hidden");
        }
    });
    if(id == 1){
        $(".suggest").removeClass("hidden");
        jmbtcbt("suggest","");
    } else {
        $(".abt").removeClass("hidden");
        jmbtcbt("abt","");
    }
   
}

function crwt(id){
    let cid = $("#c-id").val();
    if(cid == ""){
        $("#c-id").val(id);
    } else {
        $("#cmnt-" + cid).html("");
        $(".elem-" + cid).show();
    }
    $("#c-id").val(id);
    $(".elem-" + id).hide();
    let crwtx = $(".cmnt").html();
    let cwtx = crwtx.replace("{id}","-" + id);
    cwtx = cwtx.replace("{id}","-" + id);
    $("#cmnt-" + id).html(cwtx);
}

async function comment(){
    $("#comm").attr("disabled",true);
    $("#comm").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> mengirim...");
    let cid = $("#c-id").val();
    let n = "";
    let k = "";
    if(cid == ""){
         n = $("#nama").val();
         k = $("#ket").val();
    } else {
         n = $("#nama-" + cid).val();
         k = $("#ket-" + cid).val();
    }
    let fd = new FormData();
    fd.append("nama",n);
    fd.append("ket",k);
    fd.append("parent",cid);
    let ah = await fetch("<?php echo $base_url; ?>module.php/admin/commentadd",{
        method: "POST",
        body: fd
    });
    if(ah.ok){ 
        unhides("-1","Komentar Pengunjung");
    }
   
}

async function solution(){
    $("#sug").attr("disabled",true);
    $("#sug").html("menyimpan...");
    let term = $("#sug-left").val();
    let def = $("#sug-right").val();
    let btm = $("#sug-bottom").val();
    let fd = new FormData();
    fd.append("term",term);
    fd.append("def",def);
    fd.append("btm",btm);
    let k = await fetch("<?php echo $base_url; ?>entries.php/public/suggest/",{
        method: "POST",
        body: fd
    });
    if(k.ok){
        $("#sug").attr("disabled",false);
    $("#sug").html("Sarankan Terjemahan");
    alert("Saran berhasil disimpan");
    }
}

async function translates(){
    $("#right").val("mencari terjemahan...");
    let txt = $("#left").val();
    let mode = $("#mode").val();
    let ah = await fetch("<?php echo $base_url; ?>home.php/public/TranslateText/" + txt + "/mode/" + mode);
    let jk = await ah.text();
    if(ah.ok){
            $("#right").val(jk);
    }

}

async function searchs(q){
    $(".trans").addClass("hidden");

    if(q == ""){
        $(".translate").addClass("hidden");
        $(".jdl").addClass("hidden");
    } else {
        $(".translate").addClass("hidden");
    }

    
    $(".global").each(function(a,b){
            if($($(this)).hasClass("hidden")){} else {
                $($(this)).addClass("hidden");
            }
        });
    

    $(".nav-link").each(function(a,b){
        if($($(this)).hasClass("active show")){
            $($(this)).removeClass("active show");
        }
    });
    $("#pills-" + q + "-tab").addClass("active show");
    let term = (q == "")? $("#q").val() : q;
    let id = "1";
    let template = $("#template").html();
        let slots = ["{header}","{list}"];
        let mdl = "";  
          let x = await fetch("<?php echo $base_url; ?>entries.php/public/search_entry/" + id + "/q/" + term);
          let y = await x.json();
          if(x.ok){
                let mdx = "";
                let mdl2 = "";
                if(y.rows.length == 0){
                    mdl2 += "<div class='col-lg-12 destroy'><div class='card'><div class='card-img-top' style='text-align: center;padding-top: 5%;'><i class='fa fa-question-circle' style='font-size: 7em !important; color: maroon'></i></div><div class='card-body'><div class='au-task__title'><h3><center>Maaf, istilah tidak ditemukan</center></h3></div></div></div></div>";
                    mdl = mdl2;
                } else {
                    for(let j in y.rows){
                let mdl3 = "";
                mdx = template.replace(slots[0]," " + decodeURIComponent(y.rows[j].term));
                let mdl4x = (decodeURIComponent(y.rows[j].kwd)).replace(", ",",");
                let mdl4 = mdl4x.split(",");
                for(let x in mdl4){
                    let t = '"' + mdl4[x] + '"';
                    mdl3 += "&nbsp;&nbsp;<a href='#' onclick='searchs(" + t + ")' class='badge badge-success'>" + mdl4[x] + "</a>";
                }
                mdl2 = mdx.replace(slots[1],decodeURIComponent(y.rows[j].def) + "<br/><br/>" + mdl3);
                mdl += mdl2;
              }
                }
             
          }
        
          
          $(".entry").find("div.destroy").remove();
          let padding = $(".entry").html();
        $(".entry").html(mdl + padding);
        $(".entry").removeClass("hidden");
        $(".await").each(function(a,b){
            if($($(this)).hasClass("hidden")){}
            else {
                $($(this)).removeClass("hidden");
            }
        });
        $(".await-entry").addClass("hidden");


        jmbtcbt("search",term);
}

async function entries(id){
jmbtcbt("entry",id);
    $(".trans").addClass("hidden");
    $(".translate").addClass("hidden");
    $(".jdl").addClass("hidden");
   

    let template = $("#template").html();
        let slots = ["{header}","{list}"];
        let mdl = "";  
          let x = await fetch("<?php echo $base_url; ?>entries.php/public/entry/" + id);
          let y = await x.json();
          if(x.ok){
                let mdx = "";
                let mdl2 = "";
                
              for(let j in y.rows){
                let mdl3 = "";
                mdx = template.replace(slots[0]," " + decodeURIComponent(y.rows[j].term));
                let mdl4 = (decodeURIComponent(y.rows[j].kwd)).split(",");
                for(let x in mdl4){
                    let t = '"' + mdl4[x] + '"';
                    mdl3 += "&nbsp;&nbsp;<a href='#' onclick='searchs(" + t + ")' class='badge badge-success'>" + mdl4[x] + "</a>";
                }
                mdl2 = mdx.replace(slots[1],decodeURIComponent(y.rows[j].def) + "<br/><br/>" + mdl3);
                mdl += mdl2;
              }
              
          }
        
          
          $(".entry").find("div.destroy").remove();
          let padding = $(".entry").html();
        $(".entry").html(mdl + padding);
        $(".global").each(function(a,b){
            if($($(this)).hasClass("hidden")){} else {
                $($(this)).addClass("hidden");
            }
        });
        $(".entry").removeClass("hidden");
        $(".await").each(function(a,b){
            if($($(this)).hasClass("hidden")){}
            else {
                $($(this)).removeClass("hidden");
            }
        });
        $(".await-entry").addClass("hidden");

}



async function unhides(id,kat){
jmbtcbt(id,kat);
    $(".hamburger").removeClass("is-active");
          $(".navbar-mobile").hide();
          $("#" + id).removeClass("hidden");
    
        let template = $("#template").html();
        let template2 = $("#template2").html();
        let slots = ["{header}","{list}"];
        let mdl = ""; 
        if(id == "0"){
            let mdx = template.replace(slots[0],kat);
            let x = await fetch("<?php echo $base_url; ?>module.php/admin/identy");
            let y = await x.json();
          if(x.ok){
              let mdl2 = y[0].def;              
              mdl += mdx.replace(slots[1],mdl2);
          }
        } else if(id == "-1"){
            let mdx = template.replace(slots[0],kat);
            let x = await fetch("<?php echo $base_url; ?>module.php/admin/comment");
            let y = await x.text();
          if(x.ok){
              let mdl2 = y;              
              mdl += mdx.replace(slots[1],mdl2);
          }
        } else {
            let idrnd = Math.ceil(Math.random() * 10);
            let idx = (idrnd > 4)? 4 : idrnd;
            let max = template2.replace("{cwt}","overview-item--c" + idx);
            let mdx = max.replace(slots[0],"" + kat);
          let x = await fetch("<?php echo $base_url; ?>module.php/admin/subcategory");
          let y = await x.json();
          if(x.ok){
              let mdl2 = "";
              for(let j in y.rows){
                  mdl2 += (id == y.rows[j].katid)? "<a style='color: #243232 !important' href='#' onclick='subcats(\"" + y.rows[j].id + "\",\"" + y.rows[j].subkat + "\")'>" + y.rows[j].subkat + "</a>," : "";
              }
              mdl += mdx.replace(slots[1],mdl2);
              
          }
        }
          
          let padding = '<div class="col-lg-6"><div class="au-card au-card--no-shadow au-card--no-pad m-b-40 transparent"><br/><br/><span class="await await-home"><i class="fa fa-spinner fa-pulse fa-fw"></i> memuat data...</span></div></div>';
          $(".home").find("div.destroy").remove();
        $(".home").html(mdl + padding);
        $(".global").each(function(a,b){
            if($($(this)).hasClass("hidden")){} else {
                $($(this)).addClass("hidden");
            }
        });
        $(".trans").addClass("hidden");
        $(".translate").addClass("hidden");
        $(".jdl").addClass("hidden");
        $(".home").removeClass("hidden");
        $(".await").each(function(a,b){
            if($($(this)).hasClass("hidden")){}
            else {
                $($(this)).removeClass("hidden");
            }
        });
        $(".await-home").addClass("hidden");
   
}

async function subcats(id,subkat){
    jmbtcbt("subcat",id + "/" + subkat);
    let k = await fetch("<?php echo $base_url; ?>entries.php/admin/entry");
    let m = await k.json();
    if(k.ok){
        let template = $("#template").html();
        let slots = ["{header}","{list}"];
        let mdl = template.replace(slots[0],"" + subkat);
        let mdx = "";
        for(let n in m.rows){
            mdx += (m.rows[n].module == id)? "<a href='#' onclick='entries(" + m.rows[n].id + ")'>" + decodeURIComponent(m.rows[n].term) + "</a>," : "";
        }
        let mds = mdl.replace(slots[1],mdx);
       
        $(".subcat").find("div.destroy").remove();
        let padding = $(".subcat").html();
        $(".subcat").html(mds + padding);
        $(".global").each(function(a,b){
            if($($(this)).hasClass("hidden")){} else {
                $($(this)).addClass("hidden");
            }
        });
        $(".subcat").removeClass("hidden");
        $(".await").each(function(a,b){
            if($($(this)).hasClass("hidden")){}
            else {
                $($(this)).removeClass("hidden");
            }
        });
        $(".await-subcat").addClass("hidden");
    }
}

function jmbthist(){
    
    jmbtfwd = false;
    let jmbt = JSON.parse(localStorage.getItem("jmbt-hist"));
    let gwk = {rows:[]};
    let ss = {};
    let kismin = (jmbt.rows.length) - 2;
    let misq = "";
    if(jmbt.rows[kismin].gdl == ""){
        misq = jmbt.rows[kismin].gdl;
    } else if(jmbt.rows[kismin].gdl == "1"){
        kismin = (jmbt.rows.length) - 1;
        misq = jmbt.rows[kismin].gdl;
    }
    let misqn = jmbt.rows[kismin].id;
    if(misq == ""){
     ss = {id:misqn,hist:jmbt.rows[kismin].hist,q:jmbt.rows[kismin].q,gdl:1};
     gwk.rows.push(ss);
        for(let i in jmbt.rows){
            if(jmbt.rows[i].id != misqn){
                ss = {id:jmbt.rows[i].id,hist:jmbt.rows[i].hist,q:jmbt.rows[i].q,gdl:jmbt.rows[i].gdl};
                gwk.rows.push(ss);
            }
        }

     let gw = JSON.stringify(gwk);
    localStorage.removeItem("jmbt-hist");
    localStorage.setItem("jmbt-hist",gw);

    let slt = jmbt.rows[kismin].hist;
    let q = jmbt.rows[kismin].q;
    console.log(slt);
    switch(slt){
        case "suggest": 
        $(".global").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    });
        solute(1);
        break;
        case "abt": 
        $(".global").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    });
        solute(2);
        break;
        case "subcat": 
        $(".trans").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    });
    let qu = q.split("/");
    subcats(qu[0],qu[1]);
        break;
        case "search": 
        $(".trans").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    });
    searchs(q);
        break;
        case "entry": 
        $(".trans").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    });
    entries(q);
        break;
        case "home": 
        location.reload();
        break;
        default: 
        $(".trans").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    });
        unhides(slt,q);
        break;
    }
    }
}

$(document).ready(function(){
 
    let ids = Math.ceil(Math.random() * 10000);
    let jmbthist = '{"rows":[{"id":' + ids + ',"hist":"home","q":"","gdl":""}]}';
    localStorage.setItem("jmbt-hist",jmbthist);


async function get_module(){
    let a = await fetch("<?php echo $base_url; ?>module.php/public/mod");
    let b = await a.json();
    if(a.ok){
        let mdl = "";
        mdl += "<li><a href='public'><i class='fa fa-home'></i>Home</a></li>";
       /* let lnk = '"0","Balai Bahasa Provinsi Bali"';
        mdl += "<li onclick='unhides(" + lnk + ")'><a href='#'><i class='fa fa-arrow-right'></i>Tentang Balai Bahasa</a></li>"; */
        lnk = '"-1","Komentar Pengunjung"';
        mdl += "<li onclick='unhides(" + lnk + ")'><a href='#'><i class='fa fa-arrow-right'></i>Komentar Pengunjung</a></li>";
        for(let i in b.rows){
            if(b.rows[i].name == "Home"){} else {
                let lnk = '"' + b.rows[i].id + '","' + b.rows[i].name + '"';
                mdl += "<li onclick='unhides(" + lnk + ")'><a href='#'><i class='fa fa-folder'></i>" + b.rows[i].name + "</a></li>";
            }
            
        }
        $(".modules").append(mdl);
    }
}
get_module();


async function load_main(){
    let a = await fetch("<?php echo $base_url; ?>module.php/admin/category");
    let b = await a.json();
    if(a.ok){
        let template2 = $("#template2").html();
        let slots = ["{header}","{list}"];
        let mdl = "";
        for(let i in b.rows){
            let idrnd = Math.ceil(Math.random() * 10);
            let idx = (idrnd > 4)? 1 : idrnd;
            let max = template2.replace("{cwt}","overview-item--c" + idx);
            let mdx = max.replace(slots[0],"" + b.rows[i].name);
          let x = await fetch("<?php echo $base_url; ?>module.php/admin/subcategory");
          let y = await x.json();
          if(x.ok){
              let mdl2 = "";
              for(let j in y.rows){
                  mdl2 += (b.rows[i].id == y.rows[j].katid)? "<a style='color: #243232 !important' href='#' onclick='subcats(\"" + y.rows[j].id + "\",\"" + y.rows[j].subkat + "\")'>" + y.rows[j].subkat + "</a>," : "";
              }
              mdl += mdx.replace(slots[1],mdl2);
          }
        }
       
       $(".home").find("div.destroy").remove();
       let padding = $(".home").html();
        $(".home").html(mdl + padding);
        $(".await").each(function(a,b){
            if($($(this)).hasClass("hidden")){}
            else {
                $($(this)).removeClass("hidden");
            }
        });
        $(".await-home").addClass("hidden");
    }
}
//load_main();

async function record_user(){
    let k = await fetch("<?php echo $base_url; ?>visit.php/admin/getip");
}

record_user();

});
</script>

  
    <?php
    break;
}

?>

<script src="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/popper.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/bootstrap.min.js" type="text/javascript"></script>

<script src="<?php echo $base_url; ?>view/vendor/slick/slick.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/wow/wow.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/animsition/animsition.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/counter-up/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/counter-up/jquery.counterup.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/circle-progress/circle-progress.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/perfect-scrollbar/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/chartjs/Chart.bundle.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/select2/select2.min.js" type="text/javascript">
    </script>

<script src="<?php echo $base_url; ?>view/js/main.js" type="text/javascript"></script>
</body>

<!-- Mirrored from colorlib.com/polygon/cooladmin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 03:41:59 GMT -->
</html>