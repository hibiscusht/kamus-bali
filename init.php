<?php
error_reporting(E_ERROR | E_WARNING);
date_default_timezone_set("Asia/Bangkok");
setlocale(LC_ALL,"IND");
setlocale(LC_ALL,"id-ID");
require "controller.php";
require "model.php";
$rules = $_GET["rules"];
$class = $_GET["class"];
require $rules."/controller/".$class.".php";
$c = new $class();
$c->index();
?>