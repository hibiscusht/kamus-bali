<?php

class mycelium_model {
    private function conn(){
        require "config/db.php";
        require "config/db.php";
        $p = new pdo("mysql:host=".$db["mysql"]["host"].";dbname=".$db["mysql"]["dbname"],$db["mysql"]["username"],$db["mysql"]["password"]);
        return $p;
    }
    public function runQuery($sql){
        $pdo = $this->conn();
        $rs = $pdo->query($sql);
        $errors = $pdo->errorInfo();
        $id = $pdo->lastInsertId();
        $null = $rs->rowCount();
        if($id == "0"){
            if(!is_null($errors[2])){
                $set = $errors[2];
            } else {
              $rst = $rs->fetchAll(PDO::FETCH_ASSOC);
                if(empty($rst)){
                    $set = $null;
                } else {
                    $set = $rst;
                }
            }
        } else {
            $set = $id;
        }
        return $set;
    }
    public function toJSON($rs){
        $ret = '{"rows":[';
        foreach($rs as $k => $v){
            $vu = array_map(function($x){
                return $x;
            },$v); 
            $rt .= json_encode($vu).",";
        }
        $rt = substr($rt,0,strlen($rt) - 1);
        $ret .= $rt.']}';
        return $ret;
    }
}

?>